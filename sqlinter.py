#-*- coding=utf8 -*-
import MySQLdb
import json
import config
import hashlib

def hashStr(s):
    return hashlib.md5(s).hexdigest()

def htmlEscape(s): #useless
    return s.replace("<", "&lt;").replace(">", "&gt;")

def connect():
    conn = None
    try:
        conn = MySQLdb.connect(host=config.host, user=config.user, passwd=config.pwd,
                db=config.db, charset=config.charset)
    except Exception, e:
        print e
    return conn

def SignUp(username, passwd, email, name, code, usertype=3):
    conn = connect()
    if not conn:
        return False, u"服务器错误！"
    cur = conn.cursor()
    sql = """select `username`, `email` from `user` where `username`=%s or `email`=%s"""
    cur.execute(sql, (username, email))
    ret = cur.fetchall()
    if len(ret) > 0:
        if ret[0][0] == username:
            return (False, u"用户名已存在！")
        else:
            return (False, u"该邮箱已注册！")
    sql = """insert into `user` (`username`, `password`, `email`, `name`, `code`, `type`)
        values (%s, %s, %s, %s, %s, %s)"""
    cur.execute(sql, (username, hashStr(passwd), email, name, code, usertype))
    conn.commit()
    conn.close()
    return True, u"注册成功！"

def SignIn(username, passwd):
    conn = connect()
    if not conn:
        return False, u"服务器错误！"
    cur = conn.cursor()
    sql = """select `password`, `name` from user where `username`=%s"""
    cur.execute(sql, (username,))
    ret = cur.fetchall()
    if len(ret) == 0:
        return False, u"用户名不存在！"
    if ret[0][0] != hashStr(passwd):
        return False, u"密码错误！"
    conn.close()
    return True, ret[0][1] # ret[0][1] is nickname

def Confirm(username, code):
    conn = connect()
    if not conn:
        return False, u"服务器错误！"
    cur = conn.cursor()
    sql = """select `code`, `status` from `user` where `username`=%s"""
    cur.execute(sql, username)
    ret = cur.fetchall()
    if len(ret) == 0:
        return False, u"链接错误！"
    if ret[0][1] == 1:
        return False, u"链接已失效！"
    sql = """update `user` set `status`=1 where `username`=%s"""
    cur.execute(sql, username)
    conn.commit()
    conn.close()
    return True, u"激活成功！"


def GetProfile(username):
    conn = connect()
    if not conn:
        return False, u"服务器错误！"
    cur = conn.cursor()
    sql = """select `username`, `email`, `name`, `phone`, `school`, `address`, `avatar` 
        from `user` where `username`=%s"""
    cur.execute(sql, (username,))
    ret = cur.fetchone()
    conn.close()
    return True, ret

def GetMyItems(username):
    conn = connect()
    if not conn:
        return False, u"服务器错误！"
    cur = conn.cursor()
    sql = """select `id`, `name`, `description`, `imageurls` from `item` where `seller`=%s"""
    cur.execute(sql, (username,))
    ret = cur.fetchall()
    conn.close()
    return True, ret

def GetMyGoods(username):
    conn = connect()
    if not conn:
        return False, u"服务器错误！"
    cur = conn.cursor()
    sql = """select `id`, `name`, `description`, `imageurls` from `item` where `buyer`=%s"""
    cur.execute(sql, (username,))
    ret = cur.fetchall()
    conn.close()
    return True, ret

def ChangeAvatar(username, avatar):
    conn = connect()
    if not conn:
        return False, u"服务器错误！"
    cur = conn.cursor()
    sql = """update `user` set `avatar`=%s where `username`=%s"""
    cur.execute(sql, (avatar, username))
    conn.commit()
    conn.close()
    return True, ""


def GetAvatar(username):
    conn = connect()
    if not conn:
        return False, u"服务器错误！"
    cur = conn.cursor()
    sql = """select `avatar` from `user` where `username`=%s"""
    cur.execute(sql, username)
    ret = cur.fetchall()
    if len(ret) == 0:
        return False, u"用户名不存在！"
    if not ret[0][0]:
        return False, None
    conn.close()
    return True, ret[0][0]

def AddItem(name, desc, price, donation, seller, imageurls):
    conn = connect()
    if not conn:
        return False, u"服务器错误！"
    cur = conn.cursor()
    sql = """insert into item (`name`, `description`, `price`, `donation`, `seller`, `imageurls`) 
        values (%s, %s, %s, %s, %s, %s)"""
    cur.execute(sql, (name, desc, price, donation, seller, imageurls))
    conn.commit()
    conn.close()
    return True, u"添加成功！"

def UpdateProfile(username, name, phone, school, address):
    conn = connect()
    if not conn:
        return False, u"服务器错误！"
    cur = conn.cursor()
    sql = """update `user` set `name`=%s, `phone`=%s, `school`=%s, `address`=%s 
        where `username`=%s"""
    cur.execute(sql, (name, phone, school, address, username))
    conn.commit()
    conn.close()
    return True, u"更新成功！"

def GetItem(index):
    conn = connect()
    if not conn:
        return False, u"服务器错误！"
    cur = conn.cursor()
    sql = """select `name`, `description`, `price`, `donation`, `imageurls`, `seller`, `status` 
        from `item` where `id`=%s"""
    cur.execute(sql, (index,))
    ret = cur.fetchall()
    conn.close()
    if len(ret) == 0:
        return False, u"物品不存在！"
    return True, ret[0]

def AddToCart(username, itemid):
    conn = connect()
    if not conn:
        return False, u"服务器错误！"
    cur = conn.cursor()
    sql = """update `item` set buyer=%s, `ordertime`=CURRENT_TIMESTAMP where `id`=%s"""
    cur.execute(sql, (username, itemid))
    conn.commit()
    conn.close()
    return True, u"添加成功！"

def GetItems(page, num=20):
    conn = connect()
    if not conn:
        return False, u"服务器错误！"
    cur = conn.cursor()
    sql = """select `id`, `name`, `price`, `imageurls` from `item` where status=1 
        order by `id` limit %s, %s"""
    cur.execute(sql, (page*num, num))
    data = cur.fetchall()
    conn.close()
    return True, data
