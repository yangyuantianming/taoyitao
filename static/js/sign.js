function refreshcode(){
    $('#vcode').attr("src","/code?" + Math.random());
}
function signup(){
    var username = $('input[name=username]').val().trim();
    var email = $('input[name=email]').val().trim();
    var name = $('input[name=name]').val().trim();
    var password = $('input[name=password]').val().trim();
    var password2 = $('input[name=password2]').val().trim();
    var code = $('input[name=code]').val().trim();
    if(!username.match(/^[a-z][a-z0-9_-]{5,19}$/gi)){
        cloudjs.message({content:"用户名格式错误！", type:'error'});
        return;
    }
    if(!email.match(/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i)){
        cloudjs.message({content:"邮箱格式错误！", type:'error'});
        return;
    }
    if(password.length<6){
        cloudjs.message({content:"密码太短！", type:'error'});
        return;
    }
    if(password!=password2){
        cloudjs.message({content:"两次密码不一致！", type:'error'});
        return;
    }
    if(code==''){
        cloudjs.message({content:"请填写验证码！", tyep:'error'});
        return;
    }
    $.ajax({
        url: '/signup',
        type: 'post',
        data:{
            username:username,
            password:password,
            name: name,
            email:email,
            code:code
        },
        timeout: 5000,
        error: function(data){

        },
        success: function(data){
            data = eval('('+data+')');
            if(data.error){
                console.log(data.error);
                cloudjs.message({content:data.msg, type:'error'});
            }
            if(data.success){
                cloudjs.message({content:data.msg, type:'success'});
                window.setTimeout(function(){window.location.href="/";}, 1500);
            }
        }
    });
};
function signin(){
    var username = $('input[name=username]').val().trim();
    var password = $('input[name=password]').val().trim();
    if(username==""){
        cloudjs.message({content:"请输入用户名！", type:'error'});
        return;
    }
    if(password==""){
        cloudjs.message({content:"请输入密码！", type:'error'});
        return;
    }
    $.ajax({
        url: '/signin',
        type: 'post',
        data:{
            username:username,
            password:password,
        },
        timeout: 5000,
        error: function(data){
            cloudjs.message({content:"网络错误！", type:'error'});
        },
        success: function(data){
            data = eval('('+data+')');
            if(data.error){
                console.log(data.error);
                cloudjs.message({content:data.msg, type:'error'});
            }
            if(data.success){
                cloudjs.message({content:data.msg, type:'success'});
                window.setTimeout(function(){window.location.href="/";}, 1500);
            }
        }
    });
}