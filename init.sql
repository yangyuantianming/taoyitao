create database if not exists yitao;
use yitao;
create table if not exists `user` (`username` char(20) not null primary key,`password` char(32),
	`email` char(32), `phone` char(16), `name` varchar(20), `avatar` char(100), `school` varchar(32),
    `address` varchar(200),`status` int default 0, `code` char(20), 
    `signuptime` TIMESTAMP not null default CURRENT_TIMESTAMP,`type` int default 3) character set utf8;
/* 
user:
status:{0:email unconfirmed, 1:confirmed}
type:{0b1:buyer, 0b01:seller, 0b001:administrator}
*/
create table if not exists `item` (`id` int not null auto_increment primary key , `name` varchar(50), 
    `price` decimal(6, 2), `donation` decimal(6,2), `description` text, `seller` char(20), 
    `buyer` char(20), `imageurls` text, `status` int default 0, 
    `addtime` TIMESTAMP not null default CURRENT_TIMESTAMP, `ordertime` TIMESTAMP null, 
    `paytime` TIMESTAMP null) character set utf8;
/*
status:{0:uncheck by administor, 1: checked|on sell, 2:someone reserved|wait payment, 3:paid|sold}
*/



