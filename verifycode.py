#!/usr/bin/env python
# coding=utf-8

from PIL import Image, ImageDraw, ImageFont, ImageFilter
from random import random,sample,randint
import string

def createImage(w=200, h=50):
    im = Image.new("RGB", (w,h), "white")
    code = sample(string.letters + string.digits, 4)
    text = " ".join(code)
    draw = ImageDraw.Draw(im)
    fontType, fontSize = "Funky.otf", 35
    font = ImageFont.truetype(fontType, fontSize)
    draw.text((20,0), text, font=font, fill=(0,0,255))

    #paras = (0.035, 0.04, 0.01, 0.03, 0.04, 1, 0.03, 0.04)
    #im = im.transform((w,h), Image.PERSPECTIVE, paras)
    #draw = ImageDraw.Draw(im)
    for i in xrange(200):
    	x,y = randint(0,w-1), randint(0, h-1)
    	draw.point((x,y), (0,0,0))

    for i in xrange(5):
    	x1,y1,x2,y2 = randint(0,w-1),randint(0,h-1),randint(0,w-1), randint(0,h-1)
    	draw.line(((x1,y1),(x2,y2)), (0,0,0))

    im = im.filter(ImageFilter.EDGE_ENHANCE_MORE)
    im = im.filter(ImageFilter.SMOOTH)

    return im, ''.join(code)

if __name__ == "__main__":
	createImage()[0].save("x.jpg")


