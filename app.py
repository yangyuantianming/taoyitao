#-*- coding=utf-8 -*-
from flask import Flask, render_template, request, redirect
from flask import send_from_directory, make_response, url_for
from flask import session, abort
import sqlinter, mailinter
from verifycode import createImage
import StringIO
import os, time
import re, string, json
from random import sample, randint


app = Flask(__name__)

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/items")
def items():
    page = int(request.args.get("page", 0))
    ret, data = sqlinter.GetItems(page, 20)
    if not ret:
        return abort(500)
    if len(data) == 0 and page > 0:
        return redirect("/items")
    keys = ("id", "name", "price", "imageurls")
    items = map(lambda it:dict(zip(keys, it)), data)
    for it in items:
        it["imageurls"] = json.loads(it["imageurls"])
        it["url"] = "/item/%d" % (it["id"],)
    return render_template("items.html", items=items)


@app.route("/signup", methods=["GET", "POST"])
def signup():
    if request.method == "GET":
        if 'username' in session:
            return redirect(url_for("index"))
        return render_template("signup.html")
    else:
        keys = ("username", "password", "email", "name")
        data = [request.form[key] for key in keys]
        data.append(''.join(sample(string.letters, 20)))
        if request.form["code"].lower() != session.get("code",None):
            return json.dumps({"error":session["code"], "msg":u"验证码错误！"})
        if not re.match("^[A-Za-z][A-Za-z0-9_-]{5,19}$", data[0]):
            return json.dumps({"error":1, "msg":u"用户名格式错误！"})
        ret, msg = sqlinter.SignUp(*data)
        if ret:
            mailinter.confirmMail(data[2],data[0],data[-1])
            return json.dumps({"success":1, "msg":u"注册成功！"})
        else:
            return json.dumps({"error":1, "msg":msg})

@app.route("/signin", methods=["GET", "POST"])
def signin():
    if request.method == "GET":
        if 'username' in session:
            return redirect(url_for("index"))
        return render_template("signin.html")
    else:
        keys = ("username", "password")
        data = [request.form[k] for k in keys]
        ret, msg = sqlinter.SignIn(*data)
        if ret:
            session["username"] = data[0]
            session["name"] = msg or data[0] # msg is nickname where success
            return json.dumps({"success":1, "msg":u"登录成功！"})
        else:
            return json.dumps({"error":1, "msg":msg})


@app.route("/signout")
def signout():
    session.pop("username", None)
    return redirect(url_for("index"))

@app.route("/user/")
@app.route("/user/<username>")
def profile(username=None):
    if 'username' not in session:
        return redirect(url_for('signin'))
    if not username:
        username = session['username']
    ret, profile = sqlinter.GetProfile(username)
    keys = ("username", "email", "name", "phone", "school", "address", "avatar")
    profile = [x or "" for x in profile]
    profile[-1] = profile[-1] or "/static/avatar/default.jpg"
    user = dict(zip(keys, profile))
    ret, myitems = sqlinter.GetMyItems(username)
    keys = ("id", "name", "desc", "imageurls")
    items = [dict(zip(keys, it)) for it in myitems]
    for it in items:
        it["imageurls"] = json.loads(it["imageurls"])
        it["url"] = "/item/%d" % (it["id"],)
    ret, myitems = sqlinter.GetMyGoods(username)
    keys = ("id", "name", "desc", "imageurls")
    goods = [dict(zip(keys, it)) for it in myitems]
    for it in goods:
        it["imageurls"] = json.loads(it["imageurls"])
        it["url"] = "/item/%d" % (it["id"],)
    return render_template("profile.html", user=user, items=items, goods=goods)


@app.route("/confirm/<username>/<code>")
def confirm(username, code):
    ret, msg = sqlinter.Confirm(username, code)
    return msg

@app.route("/code")
def verifycode():
    im, code = createImage()
    session["code"] = code.lower()
    out = StringIO.StringIO()
    im.save(out, format="PNG")
    content = out.getvalue()
    out.close()
    resp = make_response(content)
    resp.headers['Content-Type'] = "image/png"
    return resp

@app.route("/avatar", methods=["GET", "POST"])
def uploadavatar():
    if request.method == "GET":
        return "wrong"
    else:
        try:
            ifile = request.files['file']
            out = StringIO.StringIO()
            out.write(ifile.stream.read())
            print "length of file is %d" % (len(out.getvalue()),)
            extension = os.path.splitext(ifile.filename)[1]
            if extension.lower() not in (".png", ".jpg", ".jpeg"):
                return json.dumps({"error":1, "msg":u"图片格式错误！"})
            username = session["username"]
            filename = os.path.join("static/avatar", username+extension)
            print dir(ifile)
            ifile.save(filename)
            if os.stat(filename).st_size > 2**21:
                return json.dumps({"error":1, "msg":u"图像不能大于2M！"})
            ret, msg = sqlinter.ChangeAvatar(username, "/static/avatar/%s%s" % (username, extension))
            if not ret:
                return json.dumps({"error":1, "msg":u"上传失败！"})
        except Exception, e:
            print e
            return json.dumps({"error":1, "msg":u"网络错误！"})
        return json.dumps({"success":1, "msg":u"上传成功！"})

@app.route("/additem", methods=["GET", "POST"])
def additem():
    if "username" not in session:
        return redirect(url_for("index"))
    if request.method == "GET":
        return render_template("additem.html")
    try:
        keys = ("name", "desc", "price", "donation")
        data = [request.form[k] for k in keys]
        code = request.form["code"]
        if code.lower() != session.get("code", None):
            print code.lower(), session.get("code", None)
            return json.dumps({"error":1, "msg":u"验证码错误！"})
        files = request.files.getlist("files[]")
        imageurls = []
        for i, ifile in enumerate(files):
            extension = os.path.splitext(ifile.filename)[1]
            if extension.lower() not in (".png", ".jpg", ".jpeg"):
                return json.dumps({"error":1, "msg":u"图片格式错误！"})
            filename = "static/itemimages/%s-%d-%d%s" % (time.strftime("%Y%m%d%H%M%S"),i,randint(10000, 99999),extension)
            url = "/" + filename
            ifile.save(filename)
            imageurls.append(url)
        data.append(session["username"])
        data.append(json.dumps(imageurls))
        ret, msg = sqlinter.AddItem(*data)
        if ret:
            return json.dumps({"success": 1, "msg":msg})
        else:
            return json.dumps({"error":1, "msg":msg})
    except Exception, e:
        print e
        return json.dumps({"error":1, "msg":u"服务器错误！"})
    return json.dumps({"error":1, "msg":u"服务器错误！"})

@app.route("/updateprofile", methods=["POST"])
def updateprofile():
    if "username" not in session:
        return redirect(url_for("signin"))
    keys = ("username", "name", "phone", "school", "address")
    data = [request.form[k] for k in keys]
    if data[0] != session["username"]:
        return json.dumps({"error":1, "msg":u"信息不匹配！"})
    ret, msg = sqlinter.UpdateProfile(*data)
    if ret:
        return json.dumps({"success":1, "msg":msg})
    else:
        return json.dumps({"error":1, "msg":msg})

@app.route("/item/<int:index>")
def item(index=None):
    if "username" not in session:
        return redirect(url_for("signin"))
    if not index:
        return redirect(url_for("index"))
    keys = ("name", "desc", "price", "donation", "imageurls","seller", "status")
    ret, data = sqlinter.GetItem(index)
    if not ret:
        return redirect(url_for("index"))
    item = dict(zip(keys, data))
    item["imageurls"] = json.loads(item["imageurls"])
    item["id"] = index
    return render_template("item.html", item=item)

@app.route("/addtocart", methods=["POST"])
def addtocart():
    if "username" not in session:
        return redirect(url_for("signin"))
    username = session["username"]
    itemId = request.form["id"]
    ret, msg = sqlinter.AddToCart(username, itemId)
    if ret:
        return json.dumps({"success":1, "msg":msg})
    return json.dumps({"error":1, "msg":u"网络错误！"})



@app.errorhandler(404)
def page_not_found(error):
    return "<h1>404 NOT FOUND!</h1>", 404




if __name__ == "__main__":
    app.secret_key = os.urandom(24)
    app.run(host="0.0.0.0", debug=True)

